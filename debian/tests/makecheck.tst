## debian/tests/makecheck.tst -- GAP Test script
## script format: GAP Reference Manual section 7.9 Test Files (GAP 4r8)
##
gap> TestPackageAvailability( "scscp" , "=2.4.3" , true );
"/usr/share/gap/pkg/scscp/"
gap> LoadPackage( "SmallGrp" );
true
gap> TestPackage( "scscp" );
#I  No errors detected while testing package scscp version 2.4.3
#I  using the test file `/usr/share/gap/pkg/scscp/tst/offline.tst'
true

##
## eos
